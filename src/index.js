import longPress from './getImg.js'
import photo from "./static/吕布.jpeg";

const draw = () => {
  var canvas = document.querySelector("canvas");
  var context = canvas.getContext("2d");

  // 注意.js内置的Image对象

  // 1.实例化Image对象
  var img = new Image();
  // 2.给Image对象设置src属性
  img.src = photo;
  // 3.浏览器读取图片时间  需要等待图片读取完成
  img.onload = function () {
    // 4.调用绘制图片的方法把图片绘制到canvas中

    // 4.1 把图片定位到canvas中的某一个位置
    // drawImage(图片源,图片左上角在canvas中的X坐标,图片左上角在canvas中的Y坐标)
    context.drawImage(img,10,100);

    // 4.2 把图片定位到canvas中的某一个位置 并设置大小
    // context.drawImage(图片源, 图片左上角在canvas中的X坐标, 图片左上角在canvas中的Y坐标, 图片在canvas中显示的宽, 图片在canvas中显示的高)

    // context.drawImage(img,0,0,240,300);

    // 4.3 把图片裁切出来 定位到canvas中的某一个位置并设置大小
    // context.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);

    // context.drawImage(img,0, 0, 80, 170, 250, 0, 100, 200);
  }
}
draw()







const el = document.querySelector('#draw-wrap')
console.log('el', el);



longPress(el).then(imgUrl => {
  console.log("base64编码数据：", imgUrl);

  window.confirm()

  const a = document.createElement('a')
  a.href = imgUrl
  a.download = 'base64编码数据'

  a.click()

  let imgWindow = window.open("");
  imgWindow.document.write("<img width='100%' height='100%' src=" + imgUrl + " />");
})

