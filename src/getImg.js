import html2canvas from 'html2canvas'


let element;
let timer;

const touchStart = (resolve) => {
  element.addEventListener('touchstart', function (e) {
    e.preventDefault()
    timer = setTimeout(() => {
      htmlToCanvas(resolve)
    }, 700)
  }, { passive: false })
}

const touchEnd = () => {
  element.addEventListener('touchend', function (e) {
    e.preventDefault()
    clearTimeout(timer)
  }, { passive: false })
}

const htmlToCanvas = (resolve) => {
  html2canvas(element).then(canvas => {

    const imgUrl = canvas.toDataURL("image/png", 1); // 此方法可以设置截图质量（0-1）
    resolve?.(imgUrl)
  });
}

export default (el) => {
  element = el
  return new Promise((resolve, reject) => {
    touchStart(resolve)
    touchEnd()
  })
}
